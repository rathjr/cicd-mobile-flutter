
# CI&CD Pipeline Mobile implementation

- First we need to create jenkins pipelinejob.

![Logo](https://i.ibb.co/CbY3k0v/Capture.png)

- then tick enable github 

![Logo](https://i.ibb.co/cvPK9bP/Capture.png)

- copy below pipeline to jenkins 
```bash
pipeline {
    agent any
    // declare varaible
    environment {
        linkGit = "https://github.com/apd-developers/staff_management_mobile.git"
        chatID = "767718288"
        botToken =  "bot5371006076:AAFtkwih1-l5lmlBjmg2V1hCPyPGFkqI55Q"
    }
    stages {
        stage ('sshpass to server execute shellscript') {
            steps {
    // checkout project
                git url: "${env.linkGit}" , credentialsId: "microservice"
                sh "sshpass -p 123 ssh -o StrictHostKeyChecking=no pov.bunnarath@23.94.217.108 'bash staff_management_mobile.sh '${env.linkGit} ${env.botToken} ${env.chatID} "
                
            }
        }
   }

}
```
- in pipeline above i've use pipeline integration with shellscript, so i use shhpass to remote execute my shellscript in server
- below is my shellscript

```bash
#!/bin/bash
gitLink=$1
boToken=$2
userId=$3
#--- curl telegram api for post messeage ---
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------jenkins notify new commit------'
#--- create dir and change mode ---
sudo rm -rf staff_management_mobiles
sudo mkdir staff_management_mobiles
sudo chmod 777 staff_management_mobiles && cd staff_management_mobiles

#--- clone project ---
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> clone project...'
fun_clone_project(){
reponame="$(echo $gitLink | sed -r 's/.+\/+\/([^.]+)(\.git)?/\1/')"
git clone https://Rathhallden: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@$reponame && cd $(basename $_ .git)
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> clone success'
}
#--- calling function with argument parameter
fun_clone_project $gitLink $boToken $userId

#--- run restful client test with validation ---
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> run restful client test ...'
fun_run_restful_client_test(){
flutter test test/restful_client_test.dart > restful_client_test.txt
RCT=$(grep 'All tests passed!' restful_client_test.txt)
if [ "$RCT" == "" ]
then
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> restful client test failed'
echo "restful client test failed"
cd ~ && sudo rm -rf staff_management_mobiles
exit
else
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> restful client test success'
echo "restful client test success"
fi
}
fun_run_restful_client_test $boToken $userId

#--- run todo detail widget test with validation ---
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> run todo detail widget test ...'
fun_run_todo_detail_widget_test(){
flutter test test/todo_detail_widget_test.dart > todo_detail_widget.txt
TDW=$(grep 'All tests passed!' todo_detail_widget.txt)
if [ "$TDW" == "" ]
then
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> todo detail widget test failed'
echo "todo detail widget test failed"
cd ~ && sudo rm -rf staff_management_mobiles
exit
else
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> todo detail widget test success'
echo "todo detail widget test success"
fi
}
fun_run_todo_detail_widget_test $boToken $userId

#--- build project android to apk file with validation ---
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> build project to apk file ...'
fun_build_project(){
flutter build apk > logbuild.txt
BUILD=$(grep 'Building with sound null safety' logbuild.txt)
if [ "$BUILD" == "" ]
then
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> build faild'
echo "build falied"
cd ~ && sudo rm -rf staff_management_mobiles
else
curl https://api.telegram.org/$boToken/sendMessage?chat_id=$userId -d text='------> build success, you can download apk file below'
echo "build success"
curl -F document=@"/home/pov.bunnarath/staff_management_mobiles/staff_management_mobile/build/app/outputs/flutter-apk/app-release.apk" https://api.telegram.org/$boToken/sendDocument?chat_id=$userId
fi
}
fun_build_project $boToken $userId
```
## Config Github Webhook with jenkins

- go to project and click setting > webhook > add webhook

![Logo](https://i.ibb.co/dcvhVB8/Capture.png)

- pass url of jenkins server to payload url with /github-webhook/
- change conten type to application/json
- tick enable ssl verification
- tick just the push event

![Logo](https://i.ibb.co/0Yp6WXZ/Capture.png)
